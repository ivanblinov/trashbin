#include <iostream>
#include <ctime>
using namespace std;

int DetRec(int** matrix, int n)
{
	if (n == 1)
		return matrix[0][0];
	int sum = 0;
	int decr = 0;
	int** nextMatrix;
	int sign = 1;
	for (int i = 0; i < n; i++)
	{
		int currElem = matrix[0][i];
		
		nextMatrix = new int*[n-1];
		for (int j = 1; j < n; j++)
		{
			nextMatrix[j - 1] = new int[n-1];
			for (int k = 0; k < n; k++)
			{
				if (k == i)
				{
					decr = 1;
					continue;
				}
				nextMatrix[j - 1][k - decr] = matrix[j][k];
			}
			decr = 0;
		}
		sum += sign*currElem*DetRec(nextMatrix, n - 1);
		sign *= -1;
		for (int j = 0; j < n-1; j++)
		{
			delete[] nextMatrix[j];
		}
		delete[] nextMatrix;
	}
	return sum;
}

void printMatrix(int** matrix, int n)
{
	for (int  i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout << matrix[i][j] << "\t";
		}
		cout << endl;
	}
}
int main()
{
	srand(time(NULL));
	int n;
	cin >> n;
	int** matrix = new int*[n];
	for (int i = 0; i < n; i++)
	{
		matrix[i] = new int[n];
		for (int j = 0; j < n; j++)
		{
			matrix[i][j] = rand() % 10;
		}
	}
	
	clock_t start, end;
	start = clock();
	cout << "DET= " << DetRec(matrix, n) << endl;
	end = clock();
	cout << "time: " << (double)(end - start) / CLOCKS_PER_SEC << endl;
	system("pause");
	return(0);
}