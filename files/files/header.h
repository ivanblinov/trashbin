typedef unsigned short WORD;
typedef unsigned char BYTE;
typedef long LONG;
typedef unsigned long DWORD;

#pragma pack(push,1)
struct BITMAPFILEHEADER {
	WORD bfType;
	DWORD bfSize;
	WORD bfReserved1;
	WORD bfReserved2;
	DWORD bfOffBits;
};

struct BITMAPINFOHEADER
{
	DWORD biSize;
	LONG biWidth;
	LONG biHeight;
	WORD biPlanes;
	WORD biBitCount;
	DWORD biCompression;
	DWORD biSizeImage;
	LONG biXPelsPerMeter;
	LONG biYPelsPerMeter;
	DWORD biClrUsed;
	DWORD biClrImportant;
};
struct COLOR
{
	BYTE Red;
	BYTE Green;
	BYTE Blue;
};
struct ColorImg
{
	unsigned int Width;
	unsigned int Height;
	COLOR** Data;

};


#pragma pack(pop)
//Reading image from file
//filename -- file to read from
//img -- color image structure reference
int readImgFromFile(char* filename, ColorImg& img);
//Reading image from file
//filename -- file to write to
//img -- color image structure reference
int writeImgToFile(char* filename, ColorImg& img);

ColorImg rgb2gray(ColorImg& img);

ColorImg imgBlur(ColorImg& img, unsigned int wSize=1);

void fiftyshadesofGray(ColorImg& img);

void PrintCharArray(char** arr, int arri, int arrj);