#include "header.h"
#include <iostream>
#include <fstream>
using namespace std;

int readImgFromFile(char* filename, ColorImg& img)
{
	ifstream bmpIn;
	bmpIn.open(filename, ios::binary);
	if (!bmpIn.is_open())
	{
		
		return 1;//File open error code
	}

	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;
	bmpIn.read((char*)&bmfh, sizeof(bmfh));
	bmpIn.read((char*)&bmih, sizeof(bmih));
	int offset = 4 - (3 * bmih.biWidth) % 4;
	img.Width = bmih.biWidth;
	img.Height = bmih.biHeight;
	img.Data = new COLOR*[img.Height];
	for (int i = 0; i < img.Height; i++)
	{
		img.Data[i] = new COLOR[img.Width];
		for (int j = 0; j < img.Width; j++)
		{
			bmpIn.read((char*)&img.Data[i][j], 3);
		}
		bmpIn.seekg(offset, ios::cur);
	}

	bmpIn.close();
	return 0;
}

int writeImgToFile(char* filename, ColorImg& img)
{
	ofstream bmpOut;
	bmpOut.open(filename, ios::binary);
	if (!bmpOut.is_open())
	{

		return 1;//File open error code
	}
	int offset = 4 - (3 * img.Width) % 4;

	BITMAPFILEHEADER bmfh;	
	bmfh.bfReserved1 = 0;
	bmfh.bfReserved2 = 0;
	bmfh.bfType = (((WORD)'M') << 8) | 'B';
	bmfh.bfOffBits = sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);
	
	bmfh.bfSize = bmfh.bfOffBits + 3 * img.Width*img.Height + offset*img.Height;

	BITMAPINFOHEADER bmih;
	bmih.biClrImportant = 0;
	bmih.biClrUsed = 0;
	bmih.biHeight = img.Height;
	bmih.biWidth = img.Width;
	bmih.biSize = sizeof(BITMAPINFOHEADER);
	bmih.biSizeImage = bmfh.bfSize - bmfh.bfOffBits;
	bmih.biCompression = 0;
	bmih.biPlanes = 1;
	bmih.biXPelsPerMeter = 1;//3779
	bmih.biYPelsPerMeter = 1;//3379
	bmih.biBitCount = 24;

	bmpOut.write((char*)&bmfh, sizeof(BITMAPFILEHEADER));
	bmpOut.write((char*)&bmih, sizeof(BITMAPINFOHEADER));
	BYTE* offsetArray = new BYTE[offset];
	for (int i = 0; i < offset; i++)
	{
		offsetArray[i] = 0;
	}
	for (int i = 0; i < img.Height; i++)
	{
		
		for (int j = 0; j < img.Width; j++)
		{
			bmpOut.write((char*)&img.Data[i][j], 3);
		}
		bmpOut.write((char*)offsetArray, offset);
	}
	delete[] offsetArray;
	bmpOut.close();
	return 0;
}

ColorImg rgb2gray(ColorImg& img)
{
	ColorImg grayImg;
	grayImg.Height = img.Height;
	grayImg.Width = img.Width;

	grayImg.Data = new COLOR*[img.Height];
	for (int i = 0; i < img.Height; i++)
	{
		grayImg.Data[i] = new COLOR[img.Width];
		for (int j = 0; j < img.Width; j++)
		{
			BYTE gray = ((int)img.Data[i][j].Red + img.Data[i][j].Green + img.Data[i][j].Blue) / 3;
			grayImg.Data[i][j].Red = gray;
			grayImg.Data[i][j].Green = gray;
			grayImg.Data[i][j].Blue = gray;
		}
	}

	return grayImg;
}

ColorImg imgBlur(ColorImg& img, unsigned int wSize)
{
	ColorImg blurImg;
	blurImg.Height = img.Height;
	blurImg.Width = img.Width;
	blurImg.Data = new COLOR*[blurImg.Height];

	for (int i = 0; i < img.Height; i++)
	{
		blurImg.Data[i] = new COLOR[img.Width];
		for (int j = 0; j < img.Width; j++)
			blurImg.Data[i][j] = img.Data[i][j];
	}

	unsigned int wCount = (1 + 2 * wSize) * (1 + 2 * wSize);
	for (int i = wSize; i < blurImg.Height - wSize; i++)
	{

		for (int j = wSize; j < blurImg.Width - wSize; j++)
		{
			unsigned int sum[] = { 0, 0, 0 };
			for (int s = i - wSize; s <= i + wSize; s++)
				for (int k = j - wSize; k <= j + wSize; k++)
				{
					sum[0] += img.Data[s][k].Red;
					sum[1] += img.Data[s][k].Green;
					sum[2] += img.Data[s][k].Blue;
				}


			sum[0] /= wCount;
			sum[1] /= wCount;
			sum[2] /= wCount;
			blurImg.Data[i][j].Red = sum[0];
			blurImg.Data[i][j].Green = sum[1];
			blurImg.Data[i][j].Blue = sum[2];
		}
	}

	return blurImg;
}

void fiftyshadesofGray(ColorImg& img)
{
	char** arr = new char*[img.Height];
	for (int i = 0; i < img.Height; i++)
	{
		arr[i] = new char[img.Width];
	}
	for (int i = 0; i < img.Height; i++)
	{
		for (int j = 0; j < img.Width; j++)
		{
			
			if (img.Data[i][j].Red < 85)
			{
				arr[i][j] = '@';
			}
			else if (img.Data[i][j].Red >= 85 && img.Data[i][j].Red <= 170)
			{
				arr[i][j] = '|';
			}
			else if (img.Data[i][j].Red > 170)
			{
				arr[i][j] = ' ';
			}
				
		}
	}
	PrintCharArray(arr, img.Height, img.Width);
}

void PrintCharArray(char** arr,int arri,int arrj)
{
	ofstream fout;
	fout.open("cppstudio.txt");
	for (int i = arri-1; i > 0; i--)
	{
		for (int j = 0; j < arrj; j++)
		{
			fout << arr[i][j];
		}
		fout << endl;
	}
	/*for (int i = 0; i < arri; i++)
	{
		for (int j = 0; j < arrj; j++)
		{
			fout << arr[i][j];
		}
		fout << endl;
	}*/
}