#include <iostream>
using namespace std;
double integral(double(*func)(double), double x1, double x2, double h)
{
	double sum = 0;
	while (abs(x1-x2)>h)
	{
		sum += func(x1 + h / 2)*h;
		x1 += h;
	}
	return sum;
}

double constFunc(double x)
{
	return 1;
}

int main()
{
	cout << "Integral const func: " << integral(constFunc, 0, 1, 0.1) << endl;
	system("pause");
	return(0);
}