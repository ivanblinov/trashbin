#include <iostream>
#include <cstdarg>
using namespace std;

int sum(int n=0, ...)
{
	va_list argList;
	va_start(argList, n);
	int sum = 0;
	if (n > 0)
	{
		//sum += *(&n + 1);
		sum += va_arg(argList, int);
		if (n > 1)
		{
			for (int i = 2; i <= n; i++)
			{
				//sum += *(&n + i);
				sum += va_arg(argList, int);
			}
		}
	}
	va_end(argList);
	return sum;
}
int main()
{
	cout << sum(3,0,0,1) << endl;
	system("pause");
	return(0);
}